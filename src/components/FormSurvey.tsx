import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import RadioButtonsGroup from "./RadioButtonsGroup";
import { useRouter } from 'next/router'
import { callApi } from "../utils/api";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(3),
    width: "100%"
  },
  button: {
    margin: theme.spacing(1, 1, 0, 0),
    width: "200px",
    alignSelf: "center"
  },
}));

export default function FormSurvey(props) {
  const router = useRouter();
  const classes = useStyles();
  const [value, setValue] = React.useState({
    Q1: "",
    Q2: "",
    Q3: "",
    Q4: "",
    Q5: ""
  });

  const [error, setError] = React.useState(false);

  const handleChange = (radioButtonValue: string, questionKey: string) => {
    setValue({
      ...value,
      [questionKey]: radioButtonValue
    });
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if(value["Q1"] === "" || value["Q2"] === "" || value["Q3"] === "" || value["Q4"] === "" || value["Q5"] === "") {
      setError(true);
    } else {
      let request = {
        answers: []
      };

      let questions = props.data;

      let q1 = questions.find(q => q.questionKey === "Q1");
      request.answers.push({"questionId": q1.id, grade: value["Q1"]});
      let q2 = questions.find(q => q.questionKey === "Q2");
      request.answers.push({"questionId": q2.id, grade: value["Q2"]});
      let q3 = questions.find(q => q.questionKey === "Q3");
      request.answers.push({"questionId": q3.id, grade: value["Q3"]});
      let q4 = questions.find(q => q.questionKey === "Q4");
      request.answers.push({"questionId": q4.id, grade: value["Q4"]});
      let q5 = questions.find(q => q.questionKey === "Q5");
      request.answers.push({"questionId": q5.id, grade: value["Q5"]});

      var res = await callApi(process.env.NEXT_PUBLIC_API_ENDPOINT + "/Survey", "post", request);
      router.push({
        pathname: '/result/[result]',
        query: { result: res },
      })
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <FormControl component="fieldset" className={classes.formControl}>
        {
          props.data&& props.data.map((question, index) => <RadioButtonsGroup key={index} text={question.text} questionKey={question.questionKey} onChange={handleChange} />)
        }
        <Button type="submit" variant="outlined" color="primary" className={classes.button}>
          Submit Survey!
        </Button>
        {error ? <span>Please answer all questions before submitting</span> : null}
      </FormControl>
    </form>
  );
}
import React from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import styles from "../styles/RadioButtons.module.css";

interface Props {
  text: string;
  questionKey: string;
  onChange: any;
}

export default function RadioButtonsGroup({text, questionKey, onChange}: Props) {
  const [value, setValue] = React.useState(null);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
    onChange((event.target as HTMLInputElement).value, questionKey);
  };

  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <FormLabel component="legend">{text}</FormLabel>
      </div>
      <div className={styles.innerContainer}>
            <RadioGroup aria-label="gender" name="gender1" row value={value} onChange={handleChange}>
              <FormControlLabel labelPlacement="top" value="1" control={<Radio />} label="1" />
              <FormControlLabel labelPlacement="top" value="2" control={<Radio />} label="2" />
              <FormControlLabel labelPlacement="top" value="3" control={<Radio />} label="3" />
              <FormControlLabel labelPlacement="top" value="4" control={<Radio />} label="4" />
              <FormControlLabel labelPlacement="top" value="5" control={<Radio />} label="5" />
            </RadioGroup>
      </div>
    </div>
  );
}
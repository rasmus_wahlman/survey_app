import styles from "../styles/Result.module.css";

interface Props {
  data: any;
}

export function ResultView({data}: Props) {

  const getResult = () => {
    let list = [];
    if(data) {
        const { numberOfSurveysTaken, averageGrades, answered, questions } = data;
        list.push({"text": "Questions", ansGrade: "Your answer", avgGrade: "Average Score"});
        for(let i=0; i<questions.length; i++) {
            let question = questions[i];
            let questionId = question.id;
            let avgGrade = averageGrades.find(avg => avg.questionId == questionId);
            let ans = answered.find(a => a.questionId === questionId);
            list.push({"text": question.text, ansGrade: ans.grade, avgGrade: avgGrade.averageGrade.toFixed(2)});
        }
    }
    return list;
  }

  return (
    <>
      <h1>
        Total number of surveys taken {data.numberOfSurveysTaken} and your answer below:
      </h1>
      <div className={styles.innerContainer}>
        {
          getResult().map((res, index) => (
            <div className={styles.row} key={`con_${index}`}>
              <div className={styles.column1}>
                <span key={`text_${index}`}>{res.text}</span>
              </div>
              <div className={styles.column2}>
                <span key={`grade_${index}`}>{res.ansGrade}</span>
              </div>
              <div className={styles.column2}>
                <span key={`avgGrade_${index}`}>{res.avgGrade}</span>
              </div>    
            </div>
          ))
        }
      </div>
    </>
  );
}
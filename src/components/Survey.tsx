import { useFetch } from "../hooks/useFetch";
import styles from "../styles/Survey.module.css";
import FormSurvey from "./FormSurvey";


export const Survey: React.FC<{}> = ({}) => {
  const { data, loading } = useFetch(process.env.NEXT_PUBLIC_API_ENDPOINT + "/Survey");
  return (
      <div className={styles.container}>
        <h1>Survey!</h1>
          {
            !data 
              ? "...loading" 
              : <FormSurvey data={data} />
          }
      </div>
  );
};

export default Survey;
import { useEffect, useState, useRef } from "react";

export const useFetch = (url) => {
  const isCurrent = useRef(true);
  const [state, setState] = useState({ data: null, loading: true });

  useEffect(() => {
    return () => {
      // called when the component is going to unmount
      isCurrent.current = false;
    };
  }, []);

  useEffect(() => {
    setState((state) => ({ data: state.data, loading: true }));
    const getFunction = async (url: string) => {
      const data = await fetch(url);
      const dataText = await data.json();
      if (isCurrent.current) {
          setState({ data: dataText, loading: false });
      }
    };
    getFunction(url);
  }, [url]);

  return state;
};

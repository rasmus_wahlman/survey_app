import Survey from "../components/Survey";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
      <div className={styles.container}>
          <Survey />
      </div>
  )
}

import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { ResultView } from "../../components/ResultView";
import { callApi } from "../../utils/api";
import styles from "../../styles/Home.module.css";

export const Result: React.FC<{}> = ({}) => {
    const [data, setData] = useState(null);
    const router = useRouter();
    useEffect(() => {
        if (router.query.result) {
            getData(router.query.result);
        }

    }, [router.query.result])

    const getData = async (id) => {
        var result = await callApi(process.env.NEXT_PUBLIC_API_ENDPOINT + `/Survey/${id}`, "get");
        setData(result);
    }

    return (
        <div className={styles.container}>
            {
                !data
                    ? "...loading" 
                    : <ResultView data={data} />
            }
        </div>
    );
};

export default Result;
export const callApi = async (url, method, request?) => {
  const res = await fetch(url, {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(request),
  });

  if(!res.ok) {
    throw new Error("Status returned with error");
  } else {
    return res.json();
  }
}